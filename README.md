# How to Use the DataTools4Heart Report Script

## Introduction
This section describes how to run the `dtfh_report.py` script for generating evaluation reports for original and synthetic data files.

## Requirements
- Python 3.x
- Terminal or Command Prompt

## Basic Usage
To execute the script, open your terminal and navigate to the directory where the `dtfh_report.py` script is located.

### Basic Command
The basic command to generate a report specifying paths for the original and synthetic CSV files is as follows:

```
python dtfh_report.py path_to_original.csv path_to_synthetic.csv
```

## Advanced Usage

### Select Specific Columns
To create a report targeting specific columns, use the `--columns` flag, followed by the column names:

```
python script_name.py path_to_original.csv path_to_synthetic.csv --columns column1 column2 column3
```

### Select Random Columns
For generating a report that considers a random subset of columns, use the `--num_columns` flag followed by the number of columns:

```
python script_name.py path_to_original.csv path_to_synthetic.csv --num_columns 10
```

## Example

```
python dtfh_report.py ./data/simplified_original_dtfh_data.csv ./data/synthetic_dtfh_data.csv 
```