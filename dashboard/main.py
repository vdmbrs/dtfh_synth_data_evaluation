import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from scipy.stats import ks_2samp
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.metrics import accuracy_score
from sklearn.neighbors import NearestNeighbors
import io
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.utils import ImageReader

@st.cache_data
def load_data(original_file, synthetic_file, n_samples, n_features):
    def read_file(file, file_type):
        try:
            # Read the first few lines to check the content
            content_preview = file.read(1024).decode()
            file.seek(0)  # Reset file pointer to the beginning
            st.write(f"{file_type} file preview:")
            st.code(content_preview)

            # Try to read the CSV
            df = pd.read_csv(file)
            if df.empty:
                st.error(f"The {file_type} file is empty. Please check the file and try again.")
                return None
            st.success(f"Successfully read {file_type} file. Shape: {df.shape}")
            return df
        except pd.errors.EmptyDataError:
            st.error(f"No columns to parse from the {file_type} file. Please check if the file is not empty and is a valid CSV.")
            return None
        except Exception as e:
            st.error(f"Error reading the {file_type} file: {str(e)}")
            return None

    df_orig = read_file(original_file, "original")
    df_syn = read_file(synthetic_file, "synthetic")

    if df_orig is None or df_syn is None:
        return None, None

    st.write("Original data shape (before processing):", df_orig.shape)
    st.write("Synthetic data shape (before processing):", df_syn.shape)
    
    # Only limit samples if n_samples is less than the minimum of both datasets
    if n_samples < min(len(df_orig), len(df_syn)):
        df_orig = df_orig.sample(n=n_samples, random_state=42)
        df_syn = df_syn.sample(n=n_samples, random_state=42)
    else:
        # If n_samples is larger, use all available samples
        n_samples = min(len(df_orig), len(df_syn))
        df_orig = df_orig.sample(n=n_samples, random_state=42)
        df_syn = df_syn.sample(n=n_samples, random_state=42)
    
    # Select features
    if n_features < len(df_orig.columns):
        features = df_orig.columns[:n_features]
        df_orig = df_orig[features]
        df_syn = df_syn[features]
    else:
        n_features = len(df_orig.columns)
    
    st.write("Data shape after processing:", df_orig.shape)
    st.write(f"Using {n_samples} samples and {n_features} features")
    
    return df_orig, df_syn

def plot_histogram(df_orig, df_syn, column):
    fig, ax = plt.subplots()
    ax.hist(df_orig[column], bins=30, alpha=0.5, label="Original")
    ax.hist(df_syn[column], bins=30, alpha=0.5, label="Synthetic")
    ax.set_title(f"Histogram of {column}")
    ax.legend()
    return fig

def plot_scatter(df_orig, df_syn, x_col, y_col):
    fig, ax = plt.subplots()
    ax.scatter(df_orig[x_col], df_orig[y_col], alpha=0.5, label="Original")
    ax.scatter(df_syn[x_col], df_syn[y_col], alpha=0.5, label="Synthetic")
    ax.set_xlabel(x_col)
    ax.set_ylabel(y_col)
    ax.set_title(f"Scatter Plot: {x_col} vs {y_col}")
    ax.legend()
    return fig

def plot_correlation_heatmap(df, title):
    fig, ax = plt.subplots(figsize=(10, 8))
    sns.heatmap(df.corr(), annot=True, cmap="coolwarm", ax=ax, vmin=-1, vmax=1)
    ax.set_title(title)
    return fig

def plot_pca(df_orig, df_syn):
    scaler = StandardScaler()
    pca = PCA(n_components=2)
    
    combined_data = pd.concat([df_orig, df_syn], axis=0)
    scaled_data = scaler.fit_transform(combined_data)
    pca_result = pca.fit_transform(scaled_data)
    
    fig, ax = plt.subplots()
    ax.scatter(pca_result[:len(df_orig), 0], pca_result[:len(df_orig), 1], alpha=0.5, label="Original")
    ax.scatter(pca_result[len(df_orig):, 0], pca_result[len(df_orig):, 1], alpha=0.5, label="Synthetic")
    ax.set_xlabel("First Principal Component")
    ax.set_ylabel("Second Principal Component")
    ax.set_title("PCA: Original vs Synthetic Data")
    ax.legend()
    return fig

def compute_ks_statistic(df_orig, df_syn, column):
    statistic, p_value = ks_2samp(df_orig[column], df_syn[column])
    return statistic, p_value

def discriminative_measure(df_orig, df_syn):
    X = pd.concat([df_orig, df_syn])
    y = np.concatenate([np.ones(len(df_orig)), np.zeros(len(df_syn))])
    
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
    
    models = {
        'Random Forest': RandomForestClassifier(n_estimators=100, random_state=42),
        'Logistic Regression': LogisticRegression(random_state=42),
        'Decision Tree': DecisionTreeClassifier(random_state=42)
    }
    
    results = {}
    for name, model in models.items():
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        accuracy = accuracy_score(y_test, y_pred)
        results[name] = accuracy
    
    return results

def plot_discriminative_results(results):
    fig, ax = plt.subplots(figsize=(10, 6))
    models = list(results.keys())
    accuracies = list(results.values())
    colors = ['green' if acc <= 0.6 else 'orange' if acc <= 0.8 else 'red' for acc in accuracies]
    ax.bar(models, accuracies, color=colors)
    ax.set_ylabel('Accuracy')
    ax.set_title('Discriminative Measure Results')
    plt.xticks(rotation=45)
    for i, v in enumerate(accuracies):
        ax.text(i, v, f'{v:.2f}', ha='center', va='bottom')
    plt.tight_layout()
    return fig

def plot_decision_tree(df_orig, df_syn):
    X = pd.concat([df_orig, df_syn])
    y = np.concatenate([np.ones(len(df_orig)), np.zeros(len(df_syn))])
    
    clf = DecisionTreeClassifier(max_depth=3, random_state=42)
    clf.fit(X, y)
    
    fig, ax = plt.subplots(figsize=(20, 10))
    plot_tree(clf, filled=True, feature_names=X.columns, class_names=['Synthetic', 'Original'], ax=ax)
    plt.title("Decision Tree Visualization")
    return fig

def highlight_extremes(s):
    if pd.api.types.is_numeric_dtype(s):
        return ['background-color: red' if v <= s.quantile(0.1) else 'background-color: green' if v >= s.quantile(0.9) else '' for v in s]
    else:
        return [''] * len(s)

def compute_dcr(df_orig, df_syn):
    nn = NearestNeighbors(n_neighbors=1, metric='euclidean')
    nn.fit(df_orig)
    distances, _ = nn.kneighbors(df_syn)
    return distances.flatten()

def compute_duplicates(df_orig, df_syn):
    duplicates = df_syn[df_syn.isin(df_orig.to_dict('list')).all(axis=1)]
    return len(duplicates)

def compute_k_anonymity(df, sensitive_columns):
    return df.groupby(sensitive_columns).size().min()

def generate_pdf_report(df_orig, df_syn, disc_results):
    buffer = io.BytesIO()
    c = canvas.Canvas(buffer, pagesize=letter)
    width, height = letter

    # Title
    c.setFont("Helvetica-Bold", 16)
    c.drawString(50, height - 50, "Data Comparison Report")

    # Basic statistics
    c.setFont("Helvetica-Bold", 14)
    c.drawString(50, height - 100, "Basic Statistics")
    c.setFont("Helvetica", 12)
    c.drawString(50, height - 120, f"Original data shape: {df_orig.shape}")
    c.drawString(50, height - 140, f"Synthetic data shape: {df_syn.shape}")

    # Discriminative measure
    c.setFont("Helvetica-Bold", 14)
    c.drawString(50, height - 180, "Discriminative Measure")
    c.setFont("Helvetica", 12)
    for i, (model, accuracy) in enumerate(disc_results.items()):
        c.drawString(50, height - 200 - i*20, f"{model} Accuracy: {accuracy:.4f}")

    # KS test for each feature
    c.setFont("Helvetica-Bold", 14)
    c.drawString(50, height - 280, "Kolmogorov-Smirnov Test Results")
    c.setFont("Helvetica", 12)
    for i, column in enumerate(df_orig.columns):
        statistic, p_value = compute_ks_statistic(df_orig, df_syn, column)
        c.drawString(50, height - 300 - i*20, f"{column}: statistic={statistic:.4f}, p-value={p_value:.4f}")

    # Add plots
    y_position = height - 500
    for column in df_orig.columns[:3]:  # Limit to first 3 features for brevity
        fig = plot_histogram(df_orig, df_syn, column)
        img_data = io.BytesIO()
        plt.savefig(img_data, format='png')
        img_data.seek(0)
        img = ImageReader(img_data)
        c.drawImage(img, 50, y_position, width=400, height=300)
        y_position -= 320
        if y_position < 50:
            c.showPage()
            y_position = height - 100

    c.save()
    buffer.seek(0)
    return buffer

def main():
    st.title("Original vs Synthetic Data Comparison Dashboard")

    st.sidebar.header("Data Upload and Settings")
    original_file = st.sidebar.file_uploader("Upload Original Data CSV", type="csv")
    synthetic_file = st.sidebar.file_uploader("Upload Synthetic Data CSV", type="csv")
    
    if original_file is not None and synthetic_file is not None:
        try:
            df_orig_full = pd.read_csv(original_file)
            df_syn_full = pd.read_csv(synthetic_file)
        except Exception as e:
            st.error(f"Error reading the uploaded files: {str(e)}")
            return

        max_samples = min(len(df_orig_full), len(df_syn_full))
        max_features = min(df_orig_full.shape[1], df_syn_full.shape[1])
        
        n_samples = st.sidebar.number_input("Number of samples to use", min_value=100, max_value=max_samples, value=max_samples, step=100)
        n_features = st.sidebar.number_input("Number of features to analyze", min_value=1, max_value=max_features, value=max_features, step=1)

        df_orig, df_syn = load_data(original_file, synthetic_file, n_samples, n_features)
        
        if df_orig is None or df_syn is None:
            return

        features = df_orig.columns.tolist()

        st.sidebar.header("Navigation")
        page = st.sidebar.radio("Go to", ["Overview", "Univariate Analysis", "Bivariate Analysis", "Correlation Analysis", "Dimensionality Reduction", "Statistical Tests", "Discriminative Measure", "Privacy Measures"])

        if page == "Overview":
            st.header("Dataset Overview")
            st.write("Original Data Shape:", df_orig.shape)
            st.write("Synthetic Data Shape:", df_syn.shape)
            
            st.subheader("Original Data Sample")
            st.dataframe(df_orig.head())
            
            st.subheader("Synthetic Data Sample")
            st.dataframe(df_syn.head())

            st.subheader("Summary Statistics")
            st.write("Original Data:")
            st.dataframe(df_orig.describe())
            st.write("Synthetic Data:")
            st.dataframe(df_syn.describe())

        elif page == "Univariate Analysis":
            st.header("Univariate Analysis")
            feature = st.selectbox("Select a feature", features)
            
            st.subheader(f"Histogram: {feature}")
            fig = plot_histogram(df_orig, df_syn, feature)
            st.pyplot(fig)

            st.subheader(f"Summary Statistics: {feature}")
            col1, col2 = st.columns(2)
            with col1:
                st.write("Original Data:")
                st.dataframe(df_orig[feature].describe().to_frame().T)
            with col2:
                st.write("Synthetic Data:")
                st.dataframe(df_syn[feature].describe().to_frame().T)

        elif page == "Bivariate Analysis":
            st.header("Bivariate Analysis")
            x_col = st.selectbox("Select X-axis feature", features)
            y_col = st.selectbox("Select Y-axis feature", features)
            
            st.subheader(f"Scatter Plot: {x_col} vs {y_col}")
            fig = plot_scatter(df_orig, df_syn, x_col, y_col)
            st.pyplot(fig)

        elif page == "Correlation Analysis":
            st.header("Correlation Analysis")
            
            st.subheader("Original Data Correlation Heatmap")
            fig_orig = plot_correlation_heatmap(df_orig, "Original Data Correlation")
            st.pyplot(fig_orig)
            
            st.subheader("Synthetic Data Correlation Heatmap")
            fig_syn = plot_correlation_heatmap(df_syn, "Synthetic Data Correlation")
            st.pyplot(fig_syn)

            st.subheader("Correlation Difference (Original - Synthetic)")
            diff_corr = df_orig.corr() - df_syn.corr()
            fig_diff = plot_correlation_heatmap(diff_corr, "Correlation Difference")
            st.pyplot(fig_diff)

        elif page == "Dimensionality Reduction":
            st.header("Dimensionality Reduction")
            
            st.subheader("PCA: Original vs Synthetic Data")
            fig = plot_pca(df_orig, df_syn)
            st.pyplot(fig)

        elif page == "Statistical Tests":
            st.header("Statistical Tests")
            
            st.subheader("Kolmogorov-Smirnov Test")
            feature = st.selectbox("Select a feature", features)
            statistic, p_value = compute_ks_statistic(df_orig, df_syn, feature)
            
            st.write(f"Feature: {feature}")
            st.write(f"KS Statistic: {statistic:.4f}")
            st.write(f"p-value: {p_value:.4f}")
            
            if p_value < 0.05:
                st.markdown(f"<font color='red'>The distributions are significantly different (reject null hypothesis)</font>", unsafe_allow_html=True)
            else:
                st.markdown(f"<font color='green'>The distributions are not significantly different (fail to reject null hypothesis)</font>", unsafe_allow_html=True)

        elif page == "Discriminative Measure":
            st.header("Discriminative Measure")
            results = discriminative_measure(df_orig, df_syn)
            
            st.subheader("Classification Accuracies")
            for model, accuracy in results.items():
                color = 'green' if accuracy <= 0.6 else 'orange' if accuracy <= 0.8 else 'red'
                st.markdown(f"{model} Accuracy: <font color='{color}'>{accuracy:.4f}</font>", unsafe_allow_html=True)
            
            st.subheader("Visualization of Results")
            fig = plot_discriminative_results(results)
            st.pyplot(fig)
            
            st.subheader("Decision Tree Visualization")
            fig_tree = plot_decision_tree(df_orig, df_syn)
            st.pyplot(fig_tree)
            
            st.write("An accuracy close to 0.5 indicates that the synthetic data is similar to the original data, "
                     "while an accuracy close to 1.0 suggests that the synthetic data is easily distinguishable from the original data.")
            
            overall_accuracy = np.mean(list(results.values()))
            if overall_accuracy <= 0.6:
                st.markdown("<font color='green'>Overall, the synthetic data appears to be of good quality and privacy-preserving.</font>", unsafe_allow_html=True)
            elif overall_accuracy <= 0.8:
                st.markdown("<font color='orange'>The synthetic data shows moderate similarity to the original data. Some refinement may be needed.</font>", unsafe_allow_html=True)
            else:
                st.markdown("<font color='red'>The synthetic data is easily distinguishable from the original data. Significant improvements are needed.</font>", unsafe_allow_html=True)

        elif page == "Privacy Measures":
            st.header("Privacy Measures")
            
            st.subheader("Distance to Closest Record (DCR)")
            dcr_values = compute_dcr(df_orig, df_syn)
            fig, ax = plt.subplots()
            ax.hist(dcr_values, bins=30)
            ax.set_title("Distribution of DCR Values")
            ax.set_xlabel("DCR")
            ax.set_ylabel("Frequency")
            st.pyplot(fig)
            st.write("DCR measures the distance between each synthetic record and its closest original record. "
                     "Higher values indicate better privacy as synthetic records are more distinct from original ones.")
            
            mean_dcr = np.mean(dcr_values)
            if mean_dcr > 1.0:
                st.markdown(f"<font color='green'>Mean DCR: {mean_dcr:.4f} - Good privacy preservation</font>", unsafe_allow_html=True)
            elif mean_dcr > 0.5:
                st.markdown(f"<font color='orange'>Mean DCR: {mean_dcr:.4f} - Moderate privacy preservation</font>", unsafe_allow_html=True)
            else:
                st.markdown(f"<font color='red'>Mean DCR: {mean_dcr:.4f} - Poor privacy preservation</font>", unsafe_allow_html=True)
            
            st.subheader("Number of Duplicates")
            n_duplicates = compute_duplicates(df_orig, df_syn)
            duplicate_percentage = (n_duplicates / len(df_syn)) * 100
            st.write(f"Number of exact duplicates in synthetic data: {n_duplicates}")
            st.write(f"Percentage of duplicates: {duplicate_percentage:.2f}%")
            st.write("This shows how many records in the synthetic data are exact copies of records in the original data. "
                     "Fewer duplicates generally indicate better privacy.")
            
            if duplicate_percentage <= 1:
                st.markdown("<font color='green'>Very low percentage of duplicates - Good privacy preservation</font>", unsafe_allow_html=True)
            elif duplicate_percentage <= 5:
                st.markdown("<font color='orange'>Moderate percentage of duplicates - Some privacy concerns</font>", unsafe_allow_html=True)
            else:
                st.markdown("<font color='red'>High percentage of duplicates - Significant privacy risk</font>", unsafe_allow_html=True)
            
            st.subheader("k-Anonymity")
            sensitive_columns = st.multiselect("Select sensitive columns for k-anonymity", features)
            if sensitive_columns:
                k_anonymity = compute_k_anonymity(df_syn, sensitive_columns)
                st.write(f"k-anonymity value: {k_anonymity}")
                st.write("k-anonymity measures how many records in the dataset are indistinguishable from each other "
                         "based on the selected sensitive attributes. A higher k value indicates better privacy.")
                
                if k_anonymity >= 5:
                    st.markdown("<font color='green'>Good k-anonymity - Strong privacy protection</font>", unsafe_allow_html=True)
                elif k_anonymity >= 2:
                    st.markdown("<font color='orange'>Moderate k-anonymity - Some privacy protection</font>", unsafe_allow_html=True)
                else:
                    st.markdown("<font color='red'>Poor k-anonymity - Weak privacy protection</font>", unsafe_allow_html=True)
            else:
                st.write("Please select sensitive columns to compute k-anonymity.")

        # PDF Report Generation
        if st.sidebar.button("Generate PDF Report"):
            disc_results = discriminative_measure(df_orig, df_syn)
            pdf_buffer = generate_pdf_report(df_orig, df_syn, disc_results)
            st.sidebar.download_button(
                label="Download PDF Report",
                data=pdf_buffer,
                file_name="data_comparison_report.pdf",
                mime="application/pdf"
            )

    else:
        st.write("Please upload both the original and synthetic data files to begin the analysis.")

if __name__ == "__main__":
    main()