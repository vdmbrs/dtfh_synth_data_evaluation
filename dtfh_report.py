"""
to run it please use terminal:

python dtfh_report.py path_to_original.csv path_to_synthetic.csv

# select specific columns 
python script_name.py path_to_original.csv path_to_synthetic.csv --columns column1 column2 column3

# select random columns 
python script_name.py path_to_original.csv path_to_synthetic.csv --num_columns 10

"""

import warnings

# Filter the warning of type RuntimeWarning with a message contains 'divide by zero encountered in double_scalars'
warnings.filterwarnings(
    "ignore", "divide by zero encountered in double_scalars", RuntimeWarning
)

import argparse


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import base64
import io, urllib
from datetime import datetime
import sys, os


from sklearn.datasets import fetch_california_housing
from sklearn.feature_selection import mutual_info_regression
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import plot_tree
from scipy.stats import ks_2samp
from sklearn.metrics import mean_squared_error
from sklearn.neighbors import NearestNeighbors

from scipy.stats import probplot
from scipy.stats import chi2_contingency
from scipy.stats import ttest_ind

from xgboost import XGBClassifier

from sklearn.preprocessing import OrdinalEncoder

def compute_ndr(df_orig, df_syn):
    """
    Compute the Nearest Neighbor Distance Ratio (NDR) for each row in the synthetic dataset
    with respect to the original dataset.
    """
    # Combine both datasets for nearest neighbor computation
    combined_df = pd.concat([df_orig, df_syn], ignore_index=True)
    
    # Initialize Nearest Neighbors model, finding the 2 nearest neighbors to include the closest original row
    nn = NearestNeighbors(n_neighbors=2)
    nn.fit(df_orig)
    
    # Compute distances to the two nearest neighbors in the original dataset for each synthetic row
    distances, indices = nn.kneighbors(df_syn)
    
    # Calculate NDR (distance to the nearest neighbor over distance to the second-nearest neighbor)
    ndr_values = distances[:, 0] / distances[:, 1]
    
    return ndr_values

def plot_ndr(ndr_values):
    plt.figure(figsize=(10, 6))
    plt.hist(ndr_values, bins=30, alpha=0.5, color='blue')
    plt.title('Histogram of Nearest Neighbor Distance Ratio (NDR)')
    plt.xlabel('NDR Value')
    plt.ylabel('Frequency')
    plt.tight_layout()

    # Save plot to a BytesIO object
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    buf.seek(0)

    # Convert bytes to base64 string for HTML embedding
    base64_image = base64.b64encode(buf.getvalue()).decode()
    plt.close()

    return base64_image

def compute_dcr_stats(df_orig, df_syn):
    dcr_stats = []

    for column in df_orig.columns:
        # Compute DCR for this feature only
        feature_dcr_values = compute_dcr(df_orig[[column]], df_syn[[column]])

        # Compute descriptive statistics
        min_val = np.min(feature_dcr_values)
        q25 = np.percentile(feature_dcr_values, 25)
        median = np.median(feature_dcr_values)
        q75 = np.percentile(feature_dcr_values, 75)
        max_val = np.max(feature_dcr_values)
        mean = np.mean(feature_dcr_values)
        std_dev = np.std(feature_dcr_values)

        # Append the results to the dcr_stats
        dcr_stats.append({
            'Variable': column,
            'Min_DCR': min_val,
            '25th_percentile': q25,
            'Median': median,
            '75th_percentile': q75,
            'Max_DCR': max_val,
            'Mean': mean,
            'Std_dev': std_dev
        })

    # Convert dcr_stats to DataFrame
    dcr_stats_df = pd.DataFrame(dcr_stats).set_index('Variable')
    return dcr_stats_df



def qq_plot(df_orig, df_syn):
    fig, axes = plt.subplots(nrows=len(df_orig.columns), ncols=1, figsize=(6, 4*len(df_orig.columns)))

    for i, column in enumerate(df_orig.columns):
        ax = axes[i] if len(df_orig.columns) > 1 else axes

        # Generate data for the Q-Q plot
        (osm, osr), (slope, intercept, r) = probplot(df_orig[column], dist="norm", fit=True)
        (ssm, ssr), _ = probplot(df_syn[column], dist="norm", fit=True)

        # Plot the original data Q-Q plot
        ax.plot(osm, osr, 'b.', label='Original')

        # Plot the synthetic data Q-Q plot
        ax.plot(ssm, ssr, 'r.', label='Synthetic')

        # Plot the 45-degree line
        ax.plot([np.min(osm), np.max(osm)], [np.min(osm) * slope + intercept, np.max(osm) * slope + intercept], 'g-', label='45-degree line')

        ax.legend(loc="best")
        ax.title.set_text('Q-Q plot for variable: {}'.format(column))

    plt.tight_layout()

    # Save plot to a BytesIO object
    bytes_image = io.BytesIO()
    plt.savefig(bytes_image, format='png')
    bytes_image.seek(0)

    # Convert bytes to base64 string
    base64_image = base64.b64encode(bytes_image.getvalue()).decode()

    # Close the figure
    plt.close(fig)

    return base64_image

def generate_heatmap(data, columns, title, ax):
    # Filling NaN values with 0
    filled_data = data.fillna(0)
    # Create the heatmap
    sns.heatmap(filled_data, annot=True, fmt=".2f", linewidths=.5, ax=ax, cmap='coolwarm', cbar=True)
    # Set title and adjust the size
    ax.set_title(title, fontsize=14)
    # Set the labels and rotate them for better visibility
    ax.set_xticklabels(columns, rotation=45, ha='right', fontsize=10)
    ax.set_yticklabels(columns, rotation=0, fontsize=10)
    # Remove x and y labels as they are not necessary (columns are self-explanatory)
    ax.set_xlabel('')
    ax.set_ylabel('')


def plot_dcr(df_orig, df_syn, categorical_columns=None):
    """
    Plot the histograms of Distance to Closest Record (DCR) for each feature.

    Parameters:
    - df_orig: The original DataFrame
    - df_syn: The synthetic DataFrame
    - categorical_columns: The list of categorical column names
    """

    # Compute DCR values
    dcr_values = compute_dcr(df_orig, df_syn, categorical_columns)

    # Create a new figure for each feature
    for column in df_orig.columns:
        plt.figure(figsize=(10, 6))
        
        # Compute DCR for this feature only
        feature_dcr_values = compute_dcr(df_orig[[column]], df_syn[[column]])
        
        # Plot histogram
        plt.hist(feature_dcr_values, bins=30, alpha=0.5, label='DCR', color='blue')

        plt.title('DCR histogram for feature: {}'.format(column))
        plt.xlabel('DCR Value')
        plt.ylabel('Frequency')
        plt.legend(loc='upper right')
        plt.tight_layout()
        plt.show()

def compute_dcr(df_orig, df_syn, categorical_columns=None):
    """
    Compute the Distance to Closest Record (DCR) for synthetic dataset.

    Parameters:
    - df_orig: The original DataFrame
    - df_syn: The synthetic DataFrame
    - categorical_columns: The list of categorical column names

    Returns:
    - dcr_values: An array of DCR values for synthetic dataset
    """

    # Set up nearest neighbors model
    nn = NearestNeighbors(n_neighbors=1, metric='l1')

    # Fit the model on the original data
    nn.fit(df_orig.values)

    # Find the nearest neighbor in the original data for each point in the synthetic data
    distances, _ = nn.kneighbors(df_syn.values)

    # If we have categorical features, adjust the distances
    if categorical_columns is not None:
        for column in categorical_columns:
            orig_col = df_orig[column].values
            syn_col = df_syn[column].values

            # Find the indices where the synthetic and original categorical values don't match
            mismatch_indices = np.where(orig_col != syn_col)[0]

            # Set the distances to 1 for these indices
            distances[mismatch_indices] = 1

    # Flatten the distances to get DCR values
    dcr_values = distances.flatten()

    return dcr_values


def univariate_similarity(df_orig, df_syn):
    results = []
    for column in df_orig.columns:
        orig_col = df_orig[column]
        syn_col = df_syn[column]

        # Compute RMSE
        rmse = np.sqrt(mean_squared_error(orig_col, syn_col))

        # Compute KS statistic
        ks_stat, p_val = ks_2samp(orig_col, syn_col)

        # Append the results to the summary
        results.append({
            'Variable': column,
            'RMSE': rmse,
            'KS Stat': ks_stat,
            'p-value': p_val
        })

    # Convert results to DataFrame
    results_df = pd.DataFrame(results).set_index('Variable')
    return results_df


def color_based_on_difference(val, threshold=0.1):
    if abs(val) <= threshold:
        return 'background-color: green'
    else:
        return 'background-color: red'

def color_based_on_pvalue(val):
    color = 'red' if val <= 0.05 else 'black'
    return f'color: {color}'



def basic_stats(df_orig, df_syn):
    summary = []
    p_value_interpretations = []

    for column in df_orig.columns:
        orig_col = df_orig[column]
        syn_col = df_syn[column]

        orig_mean = np.mean(orig_col)
        orig_std = np.std(orig_col)
        syn_mean = np.mean(syn_col)
        syn_std = np.std(syn_col)

        mutual_info = mutual_info_regression(orig_col.values.reshape(-1, 1), syn_col.values.reshape(-1, 1))[0]

        mean_diff = abs(orig_mean - syn_mean)
        mean_diff_relative = mean_diff / (orig_mean if orig_mean != 0 else 1e-9)  # Handle zero division
        mean_diff_relative_percentage = mean_diff_relative * 100  # Convert to percentage
        
        std_diff = abs(orig_std - syn_std)
        std_diff_relative = std_diff / (orig_std if orig_std != 0 else 1e-9)

        t_stat, p_val = ttest_ind(orig_col, syn_col, equal_var=False)
        
        if p_val < 1e-9:
            p_val_str = "Almost zero"
            significance = "Highly significant"
        elif p_val < 0.05:
            p_val_str = f"{p_val:.4f}"
            significance = "Significant"
        elif p_val < 0.1:
            p_val_str = f"{p_val:.4f}"
            significance = "Marginally significant"
        else:
            p_val_str = f"{p_val:.4f}"
            significance = "Not significant"
        
        interpretation = f"{column}: P-Value is {p_val_str}. {significance}."
        p_value_interpretations.append(interpretation)

        summary.append({
            'Variable': column,
            'Orig_Mean': orig_mean,
            'Syn_Mean': syn_mean,
            'Mean_Diff': mean_diff,
            'Mean_Diff_Relative': mean_diff_relative,
            'Mean_Diff_Relative_Percentage': mean_diff_relative_percentage,
            'Orig_Std': orig_std,
            'Syn_Std': syn_std,
            'Std_Diff': std_diff,
            'Std_Diff_Relative': std_diff_relative,
            'Mutual_Info': mutual_info,
            'T-Statistic': t_stat,
            'P-Value': p_val
        })

    summary_df = pd.DataFrame(summary).set_index('Variable')

    # Apply your styling here
    summary_df_styled = summary_df.style.applymap(color_based_on_difference, subset=['Mean_Diff', 'Mean_Diff_Relative', 'Std_Diff', 'Std_Diff_Relative'])\
                                         .applymap(color_based_on_pvalue, subset=['P-Value'])

    return summary_df, summary_df_styled, p_value_interpretations



def discriminator_measure(df_orig, df_synthetic):
    # SETUP
    df_orig['Label'] = 1
    df_synthetic['Label'] = 0
    df_combined = pd.concat([df_orig, df_synthetic], ignore_index=True)

    # Split features and labels
    X = df_combined.drop('Label', axis=1)
    y = df_combined['Label']

    # Train-test split for the discriminator
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.5, random_state=42)

    # LR
    lr_clf = LogisticRegression(max_iter=1000)
    lr_clf.fit(X_train, y_train)
    lr_pred = lr_clf.predict(X_test)
    lr_accuracy = accuracy_score(y_test, lr_pred)

    # RF
    rf_clf = RandomForestClassifier(n_jobs=-1)
    rf_clf.fit(X_train, y_train)
    rf_pred = rf_clf.predict(X_test)
    rf_accuracy = accuracy_score(y_test, rf_pred)

    # DT
    dt_clf = DecisionTreeClassifier(max_depth=3)
    dt_clf.fit(X_train, y_train)
    dt_pred = dt_clf.predict(X_test)
    dt_accuracy = accuracy_score(y_test, dt_pred)

    # XGBoost
    xgb_clf = XGBClassifier(use_label_encoder=False, eval_metric='logloss')  # Prevent warning
    xgb_clf.fit(X_train, y_train)
    xgb_pred = xgb_clf.predict(X_test)
    xgb_accuracy = accuracy_score(y_test, xgb_pred)

    # Visualization for DT
    fig, ax = plt.subplots(figsize=(9, 9))
    plot_tree(dt_clf, ax=ax, feature_names=X.columns, class_names=['Synthetic', 'Original'], fontsize=11, filled=True)
    buf = io.BytesIO()
    plt.savefig(buf, format='png', dpi=100)
    plt.close(fig)
    buf.seek(0)
    img_str = 'data:image/png;base64,' + urllib.parse.quote(base64.b64encode(buf.read()))

    return lr_accuracy, rf_accuracy, dt_accuracy, xgb_accuracy, img_str





def dataframe_to_html(df):
    header = """
    <style type="text/css">
        table {
            border-collapse: collapse;
            width: 50%;
            margin: 25px 0;
            font-size: 0.9em;
            text-align: left;
        }
        th {
            background-color: #333366;
            color: white;
            padding: 12px;
            font-weight: bold;
        }
        td {
            padding: 12px;
            border-bottom: 1px solid #dddddd;
        }
    </style>
    """
    table_html = df.to_html(classes='styled-table')
    return header + table_html



def figure_to_html(fig):
    buf = io.BytesIO()
    fig.savefig(buf, format="png")
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = "data:image/png;base64," + urllib.parse.quote(string)
    html = '<img src = "%s"/>' % uri
    return html


def create_report(
    df_orig, df_syn, original_file_name, synthetic_file_name, n_columns=10, selected_columns=None
):
    """
    MAIN FUNCTION
    """
    report = []
    
    # Check if selected_columns is None or an empty list
    if not selected_columns:
        print(f'We randomly sample {n_columns} columns')
        sampled_columns = np.random.choice(df_orig.columns, n_columns, replace=False)
    else:
        sampled_columns = selected_columns
    
    report = []

    report.append(
    """
    <html>
    <head>
    <!-- Bootstrap CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style type="text/css">
        body {
            text-align: center;
            background-color: #f3f4f6;
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            color: #333;
            line-height: 1.6;
        }
        h1, h2 {
            color: #333366;
        }
        h1 {
            margin-top: 0;
        }
        img {
            display: block;
            margin: 20px auto;
            max-width: 90%;
            height: auto;
        }
        table {
            margin: 20px auto;
            border-collapse: collapse;
            width: 90%;
        }
        th, td {
            padding: 12px 15px;
            border: 1px solid #ddd;
            text-align: left;
            font-size: 14px;
        }
        th {
            background-color: #333366;
            color: white;
        }
        td {
            background-color: #f9f9f9;
        }
        .styled-table {
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .summary {
            padding: 20px;
            background-color: #fff;
            border-radius: 10px;
            margin: 20px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        .header {
            background-color: #333366;
            color: #ffffff;
            padding: 30px 20px;
            border-radius: 5px;
            margin-bottom: 30px;
        }
        .text-left {
            text-align: left;
        }
        .text-right {
            text-align: right;
        }
        .text-center {
            text-align: center;
        }
        .alert {
            padding: 20px;
            background-color: #f44336; /* Red */
            color: white;
            margin-bottom: 15px;
        }
        .alert-green {
            background-color: #4CAF50; /* Green */
        }
        .alert-blue {
            background-color: #2196F3; /* Blue */
        }
        .closebtn {
            margin-left: 15px;
            color: white;
            font-weight: bold;
            float: right;
            font-size: 22px;
            line-height: 20px;
            cursor: pointer;
            transition: 0.3s;
        }
        .closebtn:hover {
            color: black;
        }
        .summary-message {
            font-weight: bold;
        }
        .header .header-text {
                color: #ffffff; 
        }
        .header-logo {
            float: center; /* Aligns the logo to the left */
        }
        .header {
            overflow: hidden; /* Ensures that the container accounts for the floated logo */
        }
        
    </style>

    </head>
    <body>
    <div class="header">
    """
    )

    import os
    
    # Get the current working directory
    current_directory = os.getcwd()
    
    # Assuming 'logo.png' is in the same directory as your script
    logo_path = os.path.join(current_directory, "./dtfh_synth_data_evaluation/DT4H.png")
    
    # Now you can use logo_path in your script
    with open(logo_path, "rb") as image_file:
        encoded_string = base64.b64encode(image_file.read()).decode()
        
    # Now encoded_string can be used in the src attribute
    logo_src = f"data:image/png;base64,{encoded_string}"
     
    # Current date and time
    current_datetime = datetime.now().strftime("%B %d, %Y at %H:%M")
    
    # Append the logo and header text to the report
    report.append(f"""
    <div class="header">
        <img src="{logo_src}" alt="Company Logo" class="header-logo" style="width:25%; height:auto;"/> <!-- Scale width to 50%, height will be scaled automatically -->
        <h1 class="header-text">DataTools4Heart</h1>
        <h1 class="header-text"><b>Synthetic Data Evaluation Report [W5]</b></h1>
        <h1 class="header-text">Lynkeus</h1>
        <h1 class="header-text">{current_datetime}</h1>
    </div> <!-- Close header div -->
    """)


    report.append('</div> <!-- Close header div -->')

    # Left-align using Bootstrap classes
    report.append(
        '<h2 class="text-left">Original data file name: <b>{}</b></h2>'.format(
            original_file_name
        )
    )
    report.append(
        '<h2 class="text-left">Synthetic data file name: <b>{}</b></h2>'.format(
            synthetic_file_name
        )
    )
    report.append("<hr>")

    report.append("<h2>A1 Basic Statistics</h2>")
    report.append("<p>This section presents the summary statistics for randomly sampled columns of your original and synthetic datasets.</p>")


    # Compute basic statistics
    summary_df, summary_df_styled, p_value_texts = basic_stats(df_orig[sampled_columns], df_syn[sampled_columns])
    report.append(dataframe_to_html(summary_df_styled))

    # Append p-value interpretations
    report.append("<h3>P-Value Interpretations</h3>")
    report.append("<p><strong>H0 Hypothesis:</strong> The means of the original and synthetic datasets for each variable are equal.</p>")
    report.append("<p><strong>H1 Hypothesis:</strong> The means of the original and synthetic datasets for each variable are not equal.</p>")
    report.append("<ul>")
    
    print('!!!!!!!!', p_value_texts)
    for text in p_value_texts:
        feature_name, interpretation = text.split(": ", 1)
        report.append(f"<li><strong>{feature_name}</strong>: {interpretation}</li>")
    report.append("</ul>")

    # Generate histograms, KDE, and Box Plots
    report.append("<hr>")
    report.append("<h2>A2 Data Visualization</h2>")
    
    df_orig = df_orig.fillna(0)  # Filling NaN values with 0


    for column in sampled_columns:
        fig, axes = plt.subplots(1, 2, figsize=(20, 6))

        # Histograms and KDE on the first subplot
        axes[0].hist(df_orig[column], bins=30, alpha=0.5, label="Original", color="blue", density=True)
        axes[0].hist(df_syn[column], bins=30, alpha=0.5, label="Synthetic", color="red", density=True)
        sns.kdeplot(df_orig[column], color="blue", linewidth=2, ax=axes[0])
        sns.kdeplot(df_syn[column], color="red", linewidth=2, ax=axes[0])
        axes[0].set_title(f"Histogram and KDE for {column}")
        axes[0].set_xlabel("Value")
        axes[0].set_ylabel("Density")
        axes[0].legend(loc="upper right")

        # Box Plot on the second subplot
        box_data = [df_orig[column], df_syn[column]]
        axes[1].boxplot(box_data, vert=True, patch_artist=True, labels=["Original", "Synthetic"])
        axes[1].set_title(f"Box Plot for {column}")
        axes[1].set_ylabel("Value")

        report.append(figure_to_html(fig))


    # Generate correlation heatmaps
    report.append("<hr>")
    report.append("<h2>A3 Correlation Heatmaps</h2>")
    report.append("<p>These heatmaps display the correlation between variables. A value close to 1 implies strong positive correlation, and close to -1 implies strong negative correlation.</p>")

    heatmaps_data = [
        (df_orig[sampled_columns].corr(), "Original Data Correlation"),
        (df_syn[sampled_columns].corr(), "Synthetic Data Correlation"),
        (df_orig[sampled_columns].corr() - df_syn[sampled_columns].corr(), "Difference in Correlation")
    ]

    for data, title in heatmaps_data:
        fig, ax = plt.subplots(figsize=(12, 8))  # Increased figure size for better visibility
        generate_heatmap(data, sampled_columns, title, ax)
        plt.tight_layout()
        report.append(figure_to_html(fig))
        plt.close(fig)



    # discriminator_measureS
    lr_accuracy, rf_accuracy, dt_accuracy, xgb_accuracy, dt_img_str = discriminator_measure(
        df_orig.copy(), df_syn.copy()
    )
    
    report.append("<hr>")
    report.append("<h2>A4 Discriminator Measure</h2>")
    report.append(
        "<p>The discriminator measure is a technique used to evaluate the distinguishability between "
        "original and synthetic datasets. It involves training classifiers (Logistic Regression, Random Forest, "
        "Decision Tree, and now XGBoost) on a combined dataset where original data is labeled as 1 "
        "and synthetic as 0. After a train-test split, these classifiers learn to differentiate between "
        "original and synthetic data. Their performance is then evaluated on the test data, with accuracy "
        "scores indicating how well they can discriminate between the original and synthetic data. This "
        "score provides a quantitative measure of how similar the synthetic data is to the original data. "
        "A higher accuracy means the classifiers can easily tell the difference, suggesting the synthetic "
        "data is not quite like the original. Conversely, a lower accuracy suggests the synthetic data "
        "closely mimics the original data.</p>"
    )
    
    report.append(
        "<p>Logistic Regression Accuracy: <b>{:.2f}%</b></p>".format(lr_accuracy * 100)
    )
    report.append(
        "<p>Random Forest Accuracy: <b>{:.2f}%</b></p>".format(rf_accuracy * 100)
    )
    report.append(
        "<p>Decision Tree Accuracy: <b>{:.2f}%</b></p>".format(dt_accuracy * 100)
    )
    # Include XGBoost Accuracy
    report.append(
        "<p>XGBoost Accuracy: <b>{:.2f}%</b></p>".format(xgb_accuracy * 100)
    )


    report.append(
        "<p>The decision tree classifier provides an additional check on the distinguishability of the datasets. "
        "Decision trees are simple and interpretable models that can provide insights into the key features that "
        "are being used to differentiate between the original and synthetic data. Here's the decision tree used:</p>"
    )
    report.append('<img src="{}" alt="Decision Tree">'.format(dt_img_str))

    # Add Univariate Similarity section
    report.append("<hr>")
    report.append("<h2>A5 Univariate Similarity</h2>")
    report.append(
        "<p>In this section, we evaluate the similarity between the original and synthetic datasets in terms of "
        "each individual variable (or 'univariate' similarity). We compute two metrics: the Root Mean Squared Error (RMSE) "
        "and the Kolmogorov-Smirnov (KS) statistic. RMSE measures the average magnitude of the differences between the "
        "values of the original and synthetic data, with a lower RMSE indicating more similarity. The KS statistic, on "
        "the other hand, tests whether the original and synthetic data were drawn from the same distribution, with a "
        "lower value suggesting more similarity. Alongside the KS statistic, we also report the p-value, which indicates "
        "the probability of observing the current data if the original and synthetic data were from the same distribution. "
        "A higher p-value suggests more similarity.</p>"
    )

    # Compute univariate similarity measures
    univariate_similarity_results = univariate_similarity(
        df_orig[sampled_columns], df_syn[sampled_columns]
    )

    # Add results to the report
    report.append("<h3>Results:</h3>")
    report.append(univariate_similarity_results.to_html())

    # Add Q-Q Plot section
    report.append("<hr>")
    report.append("<h2>A6 Quantile-Quantile Plot</h2>")
    report.append(
        "<p>The Quantile-Quantile (Q-Q) plot is a visual tool used to determine if two datasets come from the same "
        "distribution. The Q-Q plot compares the quantiles of the original and synthetic data against the quantiles "
        "of a normal distribution. If the original and synthetic data were sampled from the same distribution, the "
        "points in the Q-Q plot should approximately follow the 45-degree reference line.</p>"
    )

    base64_image = qq_plot(df_orig[sampled_columns], df_syn[sampled_columns])

    report.append('<img src="data:image/png;base64,{}">'.format(base64_image))

    # Assuming your dataframes are named df_orig and df_syn

    # Get the list of categorical columns
    categorical_columns = df_orig.select_dtypes(include=["object"]).columns.tolist()

    # Initialize a dictionary to store test results
    chi2_test_results = {}

    # Perform Chi-Square test for each categorical column
    for column in categorical_columns:
        # Get the contingency table
        contingency_table = pd.crosstab(df_orig[column], df_syn[column])

        # Perform the test
        chi2, p, dof, expected = chi2_contingency(contingency_table)

        # Store the results
        chi2_test_results[column] = {"Chi2 Statistic": chi2, "p-value": p}

    report.append("<h2>A7 Chi-Square Test Results</h2>")
    report.append(
        "<p>The Chi-Square test is a statistical test used to determine whether there's a significant "
        "difference between the expected frequencies and the observed frequencies in one or more categories. "
        "In the context of synthetic data, it's used to check if the synthetic data follows the same distribution "
        "as the original data for categorical features.</p>"
    )
    for column, result in chi2_test_results.items():
        report.append("<p><b>Column:</b> {}</p>".format(column))
        report.append(
            "<p>Chi2 Statistic: <b>{:.2f}</b></p>".format(result["Chi2 Statistic"])
        )
        report.append("<p>p-value: <b>{:.2f}</b></p>".format(result["p-value"]))
        
        
    if len(categorical_columns) == 0:
           report.append(
        "<p> No categorical columns were found. </p>"
    ) 

    report.append("<h2>A8 Distance to Closest Record (DCR) Measure</h2>")
    report.append(
        "<p> The Distance to Closest Record (DCR) measure is used to verify the similarity between the generated synthetic data and the original samples without being exact copies. For each synthetic record, the DCR is computed as the minimum L1 distance to the records in the original dataset. Ideally, all DCR scores should be non-zero, and their distribution should be close to that of the DCRs computed with points from the original dataset.</p>"
    )

    # Compute DCR values for each feature and plot histograms
    for column in sampled_columns:
        fig = plt.figure(figsize=(10, 6))

        # Compute DCR for this feature only
        feature_dcr_values = compute_dcr(df_orig[[column]], df_syn[[column]])

        # Plot histogram
        plt.hist(feature_dcr_values, bins=30, alpha=0.5, label="DCR", color="blue")
        plt.title("DCR histogram for feature: {}".format(column))
        plt.xlabel("DCR Value")
        plt.ylabel("Frequency")
        plt.legend(loc="upper right")
        plt.tight_layout()
        report.append(figure_to_html(fig))

    report.append("<h2>DCR Statistics</h2>")
    report.append(
        "<p>The following table presents descriptive statistics of the DCR values for each feature. These statistics include the minimum, 25th percentile, median, 75th percentile, and maximum DCR values, as well as the mean and standard deviation.</p>"
    )

    # Compute DCR statistics and convert to HTML
    dcr_stats_df = compute_dcr_stats(df_orig[sampled_columns], df_syn[sampled_columns])
    report.append(dcr_stats_df.to_html())

    
    
    ndr_values = compute_ndr(df_orig, df_syn)
    ndr_image_base64 = plot_ndr(ndr_values)

    report.append("<h2>A9 Row-wise Comparison (NDR)</h2>")
    report.append(
        "<p>This section presents a histogram of the Nearest Neighbor Distance Ratio (NDR) values, offering insight into the row-wise similarities between the original and synthetic datasets. The NDR metric reflects the closeness of each synthetic row to the original dataset in a multidimensional space. Lower NDR values indicate closer matches to the nearest neighbors within the original data.</p>"
    )
    report.append(f'<img src="data:image/png;base64,{ndr_image_base64}" alt="NDR Histogram">')

    
    
    
    
    
    
    # Define thresholds for evaluation
    low_threshold = 0.6
    mid_threshold = 0.7
    high_threshold = 0.9



    # Generate summary statistics message based on flagged_columns_df
    threshold_percentage = 5
    flagged_columns_df = summary_df[summary_df['Mean_Diff_Relative_Percentage'].abs() > threshold_percentage]
    num_flagged_columns = len(flagged_columns_df)
    if num_flagged_columns == 0:
        stats_message = '<span style="color:green;">The basic statistics indicate a strong resemblance between the original and synthetic datasets.</span> '
    else:
        stats_message = '<span style="color:red;">The basic statistics reveal that at least {} columns exhibit noticeable deviations between the original and synthetic datasets.</span> '.format(num_flagged_columns)

    # Generate accuracy message with coloring
    if lr_accuracy < low_threshold or rf_accuracy < low_threshold:
        accuracy_message = '<span style="color:green;">It\'s hard to distinguish between the original and synthetic datasets based on the classifier accuracies.</span> '
        dataset_message = '<span style="color:green;">This suggests that the synthetic dataset might closely resemble the original, making it challenging for the classifiers to differentiate.</span> '
    elif (low_threshold <= lr_accuracy < mid_threshold) or (low_threshold <= rf_accuracy < mid_threshold):
        accuracy_message = '<span style="color:green;">The classifiers show a moderate capability to distinguish between the original and synthetic datasets with accuracies of {:.2f} and {:.2f} respectively.</span> '.format(lr_accuracy, rf_accuracy)
        dataset_message = '<span style="color:green;">This indicates that there are some differences between the synthetic and original datasets, but they might not be overly pronounced.</span> '
    elif (mid_threshold <= lr_accuracy < high_threshold) or (mid_threshold <= rf_accuracy < high_threshold):
        accuracy_message = '<span style="color:red;">The classifiers show a good capability to distinguish between the original and synthetic datasets with accuracies of {:.2f} and {:.2f} respectively.</span> '.format(lr_accuracy, rf_accuracy)
        dataset_message = '<span style="color:red;">This suggests that the synthetic dataset has noticeable differences from the original.</span> '
    else:
        accuracy_message = '<span style="color:red;">Both classifiers were easily able to spot the differences between the original and synthetic datasets.</span> '
        dataset_message = '<span style="color:red;">This strongly indicates that the synthetic dataset has significant distinctions compared to the original.</span> '

    # Define DCR thresholds for evaluation
    low_dcr_threshold = 0.1
    high_dcr_threshold = 0.5
    low_dcr_std_threshold = 0.05
    high_dcr_std_threshold = 0.2

    # Extract key DCR statistics
    dcr_median = dcr_stats_df['Median'].mean()
    dcr_std_dev = dcr_stats_df['Std_dev'].mean()

    # Generate DCR message with coloring
    if dcr_median < low_dcr_threshold and dcr_std_dev < low_dcr_std_threshold:
        dcr_message = '<span style="color:green;">The DCR metrics indicate that the synthetic dataset closely resembles the original data.</span> '
    elif (low_dcr_threshold <= dcr_median < high_dcr_threshold) or (low_dcr_std_threshold <= dcr_std_dev < high_dcr_std_threshold):
        dcr_message = '<span style="color:green;">The DCR metrics reveal moderate differences between the synthetic and original datasets.</span> '
    else:
        dcr_message = '<span style="color:red;">The DCR metrics show that the synthetic dataset significantly diverges from the original data.</span> '

    # Assemble the final report summary
    report.append("<h1>Summary:</h1>")

    # Combine all messages into a single summary
    final_message = (
        stats_message
        + accuracy_message
        + dataset_message
        + dcr_message
        + ' These multiple metrics collectively inform areas for optimizing the synthetic dataset generation process.'
    )


    # Append the final summary message to the report
    report.append(final_message)

    # Display the report
    report_file = "".join(report)
    # display(HTML(''.join(report_file)))

    # Check if the directory exists. If not, create it.
    reports_directory = "./dtfh_synth_data_evaluation_reports"
    if not os.path.exists(reports_directory):
        os.makedirs(reports_directory)

    # Save the report to the specified directory.
    report_filename = "report_{}.html".format(datetime.now().strftime("%Y%m%d_%H%M"))
    report_filepath = os.path.join(reports_directory, report_filename)

    with open(report_filepath, "w") as f:
        f.write(report_file)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process CSV files.")
    parser.add_argument("original", type=str, help="Path to the original CSV file.")
    parser.add_argument("synthetic", type=str, help="Path to the synthetic CSV file.")
    parser.add_argument("--columns", type=str, nargs='+', default=None, help="List of columns to use.")
    parser.add_argument("--num_columns", type=int, default=10, help="Number of columns to randomly select.")
    parser.add_argument("--cat_columns", type=int, default=[], help="Names of categorical columns.")
    
    args = parser.parse_args()

    
    # Check file extensions
    if not args.original.endswith(".csv") or not args.synthetic.endswith(".csv"):
        print("ERROR: Both files must be in CSV format.")
        sys.exit(1)

    try:
        df_orig = pd.read_csv(args.original, index_col=0)
        df_syn = pd.read_csv(args.synthetic, index_col=0)


        encoder = OrdinalEncoder(handle_unknown='use_encoded_value', unknown_value=-1)
        
        # Fit the encoder on the original dataframe
        encoder.fit(df_orig.select_dtypes(include=['object']))
        
        # Transform the original dataframe
        df_orig_encoded = df_orig.copy()
        df_orig_encoded[df_orig.select_dtypes(include=['object']).columns] = encoder.transform(df_orig.select_dtypes(include=['object']))
        
        # Transform the synthetic dataframe
        df_syn_encoded = df_syn.copy()
        df_syn_encoded[df_syn.select_dtypes(include=['object']).columns] = encoder.transform(df_syn.select_dtypes(include=['object']))
        
        # Convert encoded data back to dataframes
        df_orig = df_orig_encoded.copy()
        df_syn = df_syn_encoded.copy()

        

        if df_orig.shape[1] != df_syn.shape[1]:
            print("ERROR: Both datasets must have the same number of columns.")
            sys.exit(1)
        if df_orig.shape[0] != df_syn.shape[0]:
            print("Both datasets should have same number of samples for the better comparasion.")
            df_syn = pd.read_csv(args.synthetic, index_col=0).sample(df_orig.shape[0])

    except Exception as e:
            print(f"Error reading CSV files: {e}")
            sys.exit(1)

        
    print(df_orig.shape)
    print(df_syn.shape)
    
    print(df_orig.head)
    
    
    create_report(df_orig, df_syn, args.original, args.synthetic, n_columns=args.num_columns, selected_columns=args.columns)
    print('\n DONE!')
